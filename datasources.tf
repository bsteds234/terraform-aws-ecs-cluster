data "aws_caller_identity" "current" {}

data "template_file" "ecs" {
  template = "${var.isLinux == 0 ? 
              file("${path.module}/ecs.tmpl") : 
              file("${path.module}/ecs-linux.tmpl")}"

  vars {
    ecs_cluster_name                = "${var.ecs_cluster_name}"
    ecs_agent_version               = "${var.ecs_agent_version}"
    cpu_unbounded                   = "${var.cpu_unbounded}"
    ecs_log_level                   = "${var.ecs_log_level}"
    ecs_agent_log_shipping          = "${var.ecs_instance_logs_to_cloudwatch}"
    cloudWatchAgentSSMParameterName = "${local.cloudWatchAgentSSMParameterName}"
  }
}

data "template_file" "ecs-linux" {
  template = "${file("${path.module}/ecs-linux.tmpl")}"

  vars {
    ecs_cluster_name                = "${var.ecs_cluster_name}"
    ecs_agent_version               = "${var.ecs_agent_version}"
    cpu_unbounded                   = "${var.cpu_unbounded}"
    ecs_log_level                   = "${var.ecs_log_level}"
    ecs_agent_log_shipping          = "${var.ecs_instance_logs_to_cloudwatch}"
    cloudWatchAgentSSMParameterName = "${local.cloudWatchAgentSSMParameterName}"
  }
}

data "template_file" "domain_connect" {
  template = "${file("${path.module}/domain_connect.tmpl")}"

  vars {
    region                         = "${var.region}"
    ssm_param_domain_join_user     = "${var.ssm_param_domain_join_user}"
    ssm_param_domain_join_password = "${var.ssm_param_domain_join_password}"
    domain_name                    = "${var.domain_name}"
    account_ou                     = "${var.account_ou}"
  }
}

data "aws_ami" "ecs_optimised_windows" {
  most_recent = true
  owners      = "${var.owners}"

  filter {
    name   = "name"
    values = ["${var.ami_name}"]
  }
}

data "aws_ami" "ecs_optimised_second_cp" {
  count = "${var.second_capacity_provider ? 1 : 0}"
  most_recent = true
  owners      = "${var.owners}"

  filter {
    name   = "name"
    values = ["${var.second_capacity_provider_ami}"]
  }
}