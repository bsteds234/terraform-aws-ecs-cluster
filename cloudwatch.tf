resource "aws_cloudwatch_log_group" "ecs_agent_logs" {
  count             = "${var.ecs_instance_logs_to_cloudwatch}"
  name              = "ecs/${var.ecs_cluster_name}/ecs-agent.log"
  retention_in_days = "${var.cloudwatch_log_retention_days}"
}

resource "aws_cloudwatch_log_group" "application" {
  count             = "${var.ecs_instance_logs_to_cloudwatch}"
  name              = "ecs/${var.ecs_cluster_name}/Application"
  retention_in_days = "${var.cloudwatch_log_retention_days}"
}

resource "aws_cloudwatch_log_group" "system" {
  count             = "${var.ecs_instance_logs_to_cloudwatch}"
  name              = "ecs/${var.ecs_cluster_name}/System"
  retention_in_days = "${var.cloudwatch_log_retention_days}"
}

data "aws_iam_policy_document" "ecs_agent_logs" {
  count = "${var.ecs_instance_logs_to_cloudwatch}"

  statement {
    sid = "1"

    actions = [
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "${aws_cloudwatch_log_group.ecs_agent_logs.arn}",
      "${aws_cloudwatch_log_group.application.arn}",
      "${aws_cloudwatch_log_group.system.arn}",
    ]
  }

  statement {
    sid = "2"

    actions = [
      "logs:PutLogEvents",
    ]

    resources = [
      "${aws_cloudwatch_log_group.ecs_agent_logs.arn}/*",
      "${aws_cloudwatch_log_group.application.arn}/*",
      "${aws_cloudwatch_log_group.system.arn}/*",
    ]
  }

  statement {
    sid = "3"

    actions = [
      "ssm:getparameter",
    ]

    resources = [
      "${aws_ssm_parameter.agent_config.arn}",
    ]
  }

  statement {
    sid = "4"

    actions = [
      "cloudwatch:putmetricdata",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "ecs_agent_logs" {
  count  = "${var.ecs_instance_logs_to_cloudwatch}"
  name   = "ecs_agent_logs"
  policy = "${data.aws_iam_policy_document.ecs_agent_logs.json}"
  role   = "${aws_iam_role.ecsInstance.name}"
}
