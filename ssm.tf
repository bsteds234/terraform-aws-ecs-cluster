locals {
  cloudWatchAgentSSMParameterName = "AmazonCloudWatch-${var.ecs_cluster_name}-${var.isLinux ? "linux" : "windows"}-cloudwatch-agent-settings"
}

resource "aws_ssm_parameter" "agent_config" {
  type = "String"
  name = "${local.cloudWatchAgentSSMParameterName}"

  value = <<EOT
    {
    "logs": {
        "logs_collected": {
            "files": {
                "collect_list": [
                    {
                        "file_path": "C:\\ProgramData\\Amazon\\ECS\\log\\ecs-agent.log",
                        "log_group_name": "ecs/${var.ecs_cluster_name}/ecs-agent.log",
                        "log_stream_name": "{instance_id}"
                    }
                ]
            },
            "windows_events": {
                "collect_list": [
                    {
                        "event_format": "xml",
                        "event_levels": [
                            "VERBOSE",
                            "INFORMATION",
                            "WARNING",
                            "ERROR",
                            "CRITICAL"
                        ],
                        "event_name": "System",
                        "log_group_name": "ecs/${var.ecs_cluster_name}/System",
                        "log_stream_name": "{instance_id}"
                    },
                    {
                        "event_format": "xml",
                        "event_levels": [
                            "VERBOSE",
                            "INFORMATION",
                            "WARNING",
                            "ERROR",
                            "CRITICAL"
                        ],
                        "event_name": "Application",
                        "log_group_name": "ecs/${var.ecs_cluster_name}/Application",
                        "log_stream_name": "{instance_id}"
                    }
                ]
            }
        }
    },
    "metrics": {
        "append_dimensions": {
            "AutoScalingGroupName": "$${aws:AutoScalingGroupName}",
            "ImageId": "$${aws:ImageId}",
            "InstanceId": "$${aws:InstanceId}",
            "InstanceType": "$${aws:InstanceType}"
        },
        "metrics_collected": {
            "LogicalDisk": {
                "measurement": [
                    "% Free Space"
                ],
                "metrics_collection_interval": 60,
                "resources": [
                    "*"
                ]
            },
            "Memory": {
                "measurement": [
                    "% Committed Bytes In Use"
                ],
                "metrics_collection_interval": 60
            }
        }
    }
}
    EOT
}
