# Change Log

## v1.0.2 (11/08/2020)

- Adds the capability to include 2 capacity providers. This can be either a Windows or linux based AMI.
- Included some extra infrastructure to allow for the extra capacity provider. Launch template, ASG.

## v1.0.1 (17/06/2020)

- Adds the cluster name to the CloudWatch Log group name. This prevents clashes when multiple clusters exist in the same account.
- Changes CloudWatch Agent config to be located in SSM param store, which is part of the fix for the log group naming issue.

## v1.0.0 BREAKING CHANGE (06/06/2020)

- Replaced usage of an ASG module from the Terraform Registry to on hosted on Gitlab. Due to naming changes in the modules the ASG will be deleted and re-created. To prevent an outage you will need to detach the current ECS instances from the Auto Scaling Group, run `Terraform Apply`, allow the new instances to join the ECS cluster, drain the old instances and finally terminate them.
- Adds new argument custom_instance_tags - these tags will be appended to the tags applied by the `tags` variable
- Adds new argument custom_volume_tags - these tags will be appended to the tags applied by the `tags` variable

## v0.8.0 (12/05/2020)

- Fixes issue where the module will throw an error because it is trying to create a policy for logging even if logging is set to false.

## v0.7.0 (11/05/2020)

- Adds CloudWatch logging for Windows ECS instances.

## v 0.6.0 (17/03/2020)

- Sets remote management IPs to be a variable.

## v 0.5.0 (30/10/2019)

- Removed/updated deprecated SSM policy for current.

## v 0.4.0 (06/09/2019)

- Fixed an issue surrounding variable use.

## v 0.3.0 (06/09/2019)

- Fixes issue with ASG tags causing an error on apply.

## v 0.2.0

- Fixes issue with invalid variable.

## v 0.1.0

- Initial Commit.
