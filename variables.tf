variable "envtype" {
  type        = "string"
  description = "the type of environment the cluster will be deployed in"
  default     = "staging"
}

variable "tags" {
  type        = "map"
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "custom_instance_tags" {
  type        = "map"
  description = "A map of tags to apply to the ECS cluster instances, these will be merged with tags provided in the tags variable"
  default     = {}
}

variable "custom_volume_tags" {
  type        = "map"
  description = "A map of tags to apply to the ECS cluster instance volumes, these will be merged with tags provided in the tags variable"
  default     = {}
}

variable "ecs_cluster_name" {
  type        = "string"
  description = "The name of the ECS Cluster"
}

variable "desired_capacity" {
  type        = "string"
  description = "The desired capacity of the ASG"
  default     = 2
}

variable "min_size" {
  type        = "string"
  description = "The minimum size of the ASG"
  default     = 0
}

variable "max_size" {
  type        = "string"
  description = "the maximum size of the ASG"
  default     = 2
}

variable "health_check_type" {
  type        = "string"
  description = "The type of healthcheck for the ASG"
  default     = "EC2"
}

variable "vpc_zone_identifier" {
  type        = "list"
  description = "The subnet ID's where the EC2 cluster nodes will be placed"
}

variable "instance_type" {
  type        = "string"
  description = "the EC2 Instance type that will be used for the cluster"
}

variable "key_name" {
  type        = "string"
  description = "the name of the EC2 Key Pair used to retrieve the administrator password through the AWS API"
}

variable "spot_price" {
  type        = "string"
  description = "If set the ASG will use spot instances at the set bid price, if not on demand instances are used"
  default     = ""
}

variable "domain_name_servers_CIDR" {
  type        = "list"
  description = "DNS servers for the AD domain"
}

variable "root_volume_size" {
  type        = "string"
  description = "Sets the root volume size for the containers instances"
  default     = "200"
}

variable "volume_type" {
  type        = "string"
  description = "Set the volume type for the container instances"
}

variable "aws_alb_sg_id" {
  type        = "string"
  description = "The Security Group ID of the ALB that front traffic for the ECS Instances"
}

variable "vpc_id" {
  type        = "string"
  description = "The ID of the VPC the cluster will be placed in"
}

variable "ecs_agent_version" {
  type        = "string"
  default     = "v1.26.0"
  description = "The version of the ECS agent to deploy"
}

variable "associate_public_ip_address" {
  type        = "string"
  default     = 0
  description = "If enabled the EC2 instances will recieve public IP addresses"
}

variable "ami_name" {
  type        = "string"
  default     = "amzn2-ami-ecs-hvm"
  description = "The AMI to use for the EC2 Cluster Nodes"
}

variable "second_capacity_provider_ami" {
  type        = "string"
  default     = "amzn2-ami-ecs-hvm"
  description = "The AMI to use for the second capacity provider cluster Nodes"
}

variable "ssm_managed" {
  type        = "string"
  default     = "0"
  description = "If enabled the SSM managed policy will be applied to the cluster nodes"
}

variable "cpu_unbounded" {
  type        = "string"
  default     = "0"
  description = "Allows tasks set to 0 cpu units to be placed on a container instance"
}

variable "region" {
  type        = "string"
  default     = "eu-west-1"
  description = "The region that will be set as the shell default on the instance"
}

variable "ssm_param_domain_join_user" {
  type        = "string"
  default     = ""
  description = "The name of the SSM parameter that contains the username that will be used to authenticate the domain join"
}

variable "ssm_param_domain_join_password" {
  type        = "string"
  default     = ""
  description = "The name of the SSM parameter that contains the password that wil be used to authenticate the domain join"
}

variable "domain_name" {
  type        = "string"
  default     = ""
  description = "The name of the AD Domain to join"
}

variable "join_domain" {
  type        = "string"
  default     = "0"
  description = "Set if the the cluster is part of the Active Directory Domain."
}

variable "account_ou" {
  type        = "string"
  default     = "DC=contso,DC=com"
  description = "The name of the OU that the computer account will be created in. Defined in RFC1779 format"
}

variable "owners" {
  type        = "list"
  default     = ["amazon"]
  description = "List of AMI owners to limit search. At least 1 value must be specified. Valid values: an AWS account ID, self (the current account), or an AWS owner alias (e.g. amazon, aws-marketplace, microsoft)."
}

variable "isLinux" {
  type        = "string"
  default     = "0"
  description = "If enabled then depicts that the ECS cluster is using Linux"
}

variable "ecs_log_level" {
  type        = "string"
  description = "The level of debugging for ecs logging"
  default     = "info"
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = "string"
  default     = "ao-cluster-asg-second-capacity-provider"
}

variable "remote_managament_ips" {
  description = "A list of IP's to grant remote management access to"
  type        = "list"
  default     = ["10.10.60.0/23", "10.10.70.0/23", "10.250.248.0/21", "109.111.215.168/32"]
}

variable "ecs_instance_logs_to_cloudwatch" {
  description = "If enabled the ECS Instances will ship ECS Agent logs to CloudWatch"
  default     = false
}

variable "cloudwatch_log_retention_days" {
  description = "The number of days to retain CloudWatch Logs - valid values are 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  default     = 30
}

variable "capacity_provider" {
  description = "If this is active it will create a capacity provider along with the asg from the asg module"
  default = 0
}

variable "second_capacity_provider" {
    description = "If this is active it will create a capacity prover along with the second asg"
    default = 0
}

variable "capacity_providers" {
  description = "This is a list of capacity providers that must be listed when calling the ECS module"
  default = []
}

variable "subnets" {
  description = "These are the subnets that will be used by the instances spawned from the second asg/launch template"
  default = []
}

variable "ebs_optimized" {
  description = "Turns on EBS optimisation."
  default = "true"
}

variable "target_group_arns" {
  description = "Ihese are the target group arns"
  type    = "list"
  default = []
}